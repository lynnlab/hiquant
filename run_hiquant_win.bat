REM	run Hiquant with 1000 mb of memory GUI mode add/remove "REM" to execute/hide a line^
java -Xmx1000m -jar hiquant.jar


REM	run Hiquant with 1000 mb of RAM in command line mode^								
REM java -Xmx1000m -jar hiquant.jar config/silac.config


REM	run Hiquant with 1000 mb of RAM in command line mode silently (progress not output)^
REM java -Xmx1000m -jar hiquant.jar config/silac.config -s